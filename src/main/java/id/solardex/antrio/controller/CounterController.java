package id.solardex.antrio.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.solardex.antrio.model.Counter;
import id.solardex.antrio.dao.CounterRepository;

@Controller
@RequestMapping("/counter")
public class CounterController {

    @Autowired
    CounterRepository counterRepository;

    @GetMapping("/create")
    public String create(Counter counter) {
        return "counter/create";
    }

    @PostMapping("/save")
    public String save(@Valid Counter counter, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "counter/create";
        }
        
        counterRepository.save(counter);
        return "redirect:/counter/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("counters", counterRepository.findAll());
        return "counter/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Counter counter = counterRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid counter Id:" + id));
        
        model.addAttribute("counter", counter);
        return "counter/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Counter counter, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            counter.setId(id);
            return "counter/edit";
        }
        counterRepository.save(counter);
        return "redirect:/counter/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Counter counter = counterRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid counter Id:" + id));
        counterRepository.delete(counter);
        return "redirect:/counter/index";
    }

}
