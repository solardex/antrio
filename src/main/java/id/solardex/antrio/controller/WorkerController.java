package id.solardex.antrio.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import id.solardex.antrio.model.Worker;
import id.solardex.antrio.dao.CounterRepository;
import id.solardex.antrio.dao.WorkerRepository;

@Controller
@RequestMapping("/worker")
public class WorkerController {

    @Autowired
    WorkerRepository workerRepository;
    @Autowired
    CounterRepository counterRepository;

    @GetMapping("/create")
    public String create(Worker worker, Model model) {
    	model.addAttribute("counters", counterRepository.findAll());
        return "worker/create";
    }

    @PostMapping("/save")
    public String save(@Valid Worker worker, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "worker/create";
        }
        
        workerRepository.save(worker);
        return "redirect:/worker/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("workers", workerRepository.findAll());
        return "worker/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
    	model.addAttribute("counters", counterRepository.findAll());
    	
        Worker worker = workerRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid Worker Id:" + id));
        
        model.addAttribute("worker", worker);
        return "worker/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Worker worker, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
        	worker.setId(id);
            return "worker/edit";
        }
        workerRepository.save(worker);
        return "redirect:/worker/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Worker worker = workerRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid Worker Id:" + id));
        workerRepository.delete(worker);
        return "redirect:/worker/index";
    }

}
