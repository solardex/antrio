package id.solardex.antrio.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.solardex.antrio.model.Counter;

public interface CounterRepository extends JpaRepository<Counter, Long> {

}
