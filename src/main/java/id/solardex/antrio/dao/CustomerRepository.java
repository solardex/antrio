package id.solardex.antrio.dao;

import java.util.List;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;

import id.solardex.antrio.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    // @Query("SELECT u FROM User u WHERE u.name LIKE %?1%"
    //         + " OR u.email LIKE %?1%"
    // )
    // public List<User> contains(String keyword);
	
	List<Customer> findByCode(String code);

}
