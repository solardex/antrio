package id.solardex.antrio.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.solardex.antrio.model.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long> {


}
