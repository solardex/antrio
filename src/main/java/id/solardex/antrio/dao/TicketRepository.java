package id.solardex.antrio.dao;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;

import id.solardex.antrio.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
}
