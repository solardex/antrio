package id.solardex.antrio.model;

import javax.persistence.*;
// import javax.persistence.PrePersist;
// import javax.persistence.MappedSuperclass;

import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
// import java.util.UUID;

import lombok.Data;
@Data 
@MappedSuperclass
public class BaseModel {
    @Column(unique = true, length=35)
    @NotBlank(message = "Code is mandatory")
    private String code;

    @Column(length=100)
    @NotBlank(message = "Name is mandatory")
    private String name; 
    
	public BaseModel(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
