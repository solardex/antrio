package id.solardex.antrio.model;

import javax.persistence.*;
// import javax.persistence.PrePersist;

import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
// import java.util.UUID;

import lombok.Data;
@Data 
@Entity
public class Counter extends BaseModel {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
        name = "sequence-generator"
        , strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator"
        , parameters = {
            @Parameter(name = "sequence_name", value = "counter_seq")
            , @Parameter(name = "initial_value", value = "1")
            , @Parameter(name = "increment_size", value = "1")
        }
    )
    private long id;

    public Counter() {
        super("", "");
    }

    public Counter(String code, String name) {
        super(code, name);
    }
	
}
