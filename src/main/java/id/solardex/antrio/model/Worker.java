package id.solardex.antrio.model;

import javax.persistence.*;
// import javax.persistence.PrePersist;
// import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
// import java.util.UUID;

import lombok.Data;

@Entity
@Data 
@Table(
	uniqueConstraints = { 
		@UniqueConstraint(
			name = "UQ_worker_code", columnNames = { "code" }
		)
	}
)
public class Worker extends BaseModel {
	
	// worker can be doctor, customer service, teller, etc
	
    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
        name = "sequence-generator"
        , strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator"
        , parameters = {
            @Parameter(name = "sequence_name", value = "worker_seq")
            , @Parameter(name = "initial_value", value = "1")
            , @Parameter(name = "increment_size", value = "1")
        }
    )
    private long id;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name="dob")
    private Date dob;
    
    public Worker() {
    	super("", "");
    }
    
    @ManyToOne
    @JoinColumn(name="counter_id")
    private Counter counter;
    
}
