package id.solardex.antrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(AntrioApplication.class, args);
	}

}
