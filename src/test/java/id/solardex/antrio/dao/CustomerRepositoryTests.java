package id.solardex.antrio.dao;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.solardex.antrio.model.Customer;

@SpringBootTest
public class CustomerRepositoryTests {
	
	@Autowired CustomerRepository customerRepo;
	
	private String customerCode = "TEST-001";
	
	@Test
	@Order(1)
	public void createCustomer() {
		// create
		Customer cust = new Customer();
		cust.setCode(customerCode);
		cust.setName("CUSTOMER TESTING");
		cust.setPhone("08123456789");
		Assertions.assertEquals(cust.getId(), 0); 
		customerRepo.save(cust);
		Assertions.assertNotEquals(cust.getId(), 0);

		// read
		List<Customer> listCust = customerRepo.findByCode(customerCode);
		Assertions.assertEquals(1, listCust.size());

		// delete
		customerRepo.delete(listCust.get(0));
		listCust = customerRepo.findByCode(customerCode);
		Assertions.assertEquals(listCust.size(), 0); 
		
	}

//	@Test
//	@Order(2)
//	public void readCustomer() {
//		List<Customer> listCust = customerRepo.findByCode(customerCode);
//		Assertions.assertEquals(1, listCust.size());
//	}

//	@Test
//	@Order(2)
//	public void deleteCustomer() {
//		List<Customer> listCust = customerRepo.findByCode(customerCode);
//		customerRepo.delete(listCust.get(0));
//		listCust = customerRepo.findByCode("TEST-001");
//		Assertions.assertEquals(listCust.size(), 0); 
//	}

}
